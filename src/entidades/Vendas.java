package entidades;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Vendas implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer numeroDeVenda;
	private String nomeDoCliente;
	private Date dataDaVenda;
	private Double valorDaVenda;

	SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");

	public Vendas(Integer numeroDeVenda, String nomeDoCliente, Date dataDaVenda, Double valorDaVenda) {
		this.numeroDeVenda = numeroDeVenda;
		this.nomeDoCliente = nomeDoCliente;
		this.dataDaVenda = dataDaVenda;
		this.valorDaVenda = valorDaVenda;
	}

	public Integer getNumeroDeVenda() {
		return numeroDeVenda;
	}

	public void setNumeroDeVenda(Integer numeroDeVenda) {
		this.numeroDeVenda = numeroDeVenda;
	}

	public String getNomeDoCliente() {
		return nomeDoCliente;
	}

	public void setNomeDoCliente(String nomeDoCliente) {
		this.nomeDoCliente = nomeDoCliente;
	}

	public Date getDataDaVenda() {
		return dataDaVenda;
	}

	public void setDataDaVenda(Date dataDaVenda) {
		this.dataDaVenda = dataDaVenda;
	}

	public Double getValorDaVenda() {
		return valorDaVenda;
	}

	public void setValorDaVenda(Double valorDaVenda) {
		this.valorDaVenda = valorDaVenda;
	}

	@Override
	public String toString() {
		return "Vendas [Numero de Venda: " + numeroDeVenda + ", Nome do Cliente: " + nomeDoCliente + ", Data da Venda: "
				+ data.format(dataDaVenda) + ", Valor da Venda: R$ " + valorDaVenda + "]";

	}

}