package aplicacao;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

import entidades.Validator;

public class Programa {

	public static void main(String[] args) throws ParseException, IOException {
		Scanner sc = new Scanner(System.in);
		Validator vali = new Validator();

		System.out.println("Digite o caminho do diretório 'PENDENTES'");
		String caminho_diretorio = sc.nextLine();
		System.out.println("Digite o caminho do diretório 'VALIDADO'");
		vali.dir_validado = sc.nextLine();
		System.out.println("Digite o caminho do diretório 'INVALIDADO'");
		vali.dir_invalidado = sc.nextLine();
		
		System.out.println();
		System.out.println();
		System.out.println("------------------ARQUIVOS MOVIDOS--------------------'");

		vali.processarDiretorio(caminho_diretorio);

		sc.close();
	}

}
