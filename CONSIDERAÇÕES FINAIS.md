# TesteEffetiveLuisFilipe

Agradeço desde já a participação no processo seletivo e por até aqui ter chegado, e ficaria feliz demais em ser selecionado. 
Sei que tenho muito a melhorar e estou me esforçando para isso, esta oportunidade é de grande valor para a minha carreira e espero ter me saído bem.
Sei que o código poderia ter ficado mais otimizado, mas levando em consideração tamanha ansiedade para que tudo der certo, a responsabilidade aumenta.
Entretanto espero que meu código tenha atendido aos requisitos solicitado.



No projeto foi criado duas classes: "VENDAS" e "PROGRAMAS";

Na classe VENDAS foi criado um construtor com a instanciação de cada objeto referente ao arquivo que será analizado;

Foi adicionado também a classe "VENDAS" um toString() para converter os dados de saída de forma legivel para o usuário.

Na classe "PROGRAMA" que é o programa principal, foi criado uma lógica onde o usuário adiciona o caminho onde se encontra o arquivo CSV;

Após a inserção do caminho, o programa verificará se os dados do arquivo inserido é válido ou inválido;

Se o arquivo tiver todas as colunas exigidas na classe "VENDAS" o programa exibirá os dado do arquivo na tela, validará o arquivo transferido-o para uma pasta de nome "\VALIDADO" (este diretório deverá ter seu caminho informado pelo o usuário);

Se o arquivo não tiver as colunas exigidas na classe "VENDAS", será considerado inválido e será transferido para uma pasta de nome "\INVALIDADO"(que também deverá ser informado pelo o usuário);

Se o arquivo estiver vazio, será considerado inválido e também será transferido para a pasta de nome "\INVALIDADO";
