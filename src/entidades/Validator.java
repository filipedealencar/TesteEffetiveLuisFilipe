package entidades;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Validator {

	public String dir_invalidado;
	public String dir_validado;

	public Validator() {
	}

	public void salvarArquivo(String caminho) throws ParseException, IOException {
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");

		File pendentes = new File(caminho);

		List<Vendas> lista = new ArrayList<Vendas>();

		try (BufferedReader br = new BufferedReader(new FileReader(caminho))) {

			String linha = br.readLine();
			while (linha != null) {
				if (linha.length() > 0) {
					String[] vect = linha.split(";");
					Integer numeroDeVenda = Integer.parseInt(vect[0]);
					String nomeDoCliente = vect[1];
					Date dataDaVenda = data.parse(vect[2]);
					Double valorDaVenda = Double.parseDouble(vect[3].replace(",", "."));

					Vendas vend = new Vendas(numeroDeVenda, nomeDoCliente, dataDaVenda, valorDaVenda);
					lista.add(vend);

				}
				linha = br.readLine();

			}

		} catch (ArrayIndexOutOfBoundsException | NumberFormatException | IOException | ParseException e) {
			
			
			if (!pendentes.getName().contains("csv")) {
				pendentes.getName().substring(pendentes.getName().lastIndexOf(".") + 1);
				if(!pendentes.isDirectory());
			}else {

			if (!pendentes.exists()) {

			} else {
				// * ADICIONE ABAIXO O CAMINHO DA PASTA INVALIDADO * //
				File invalidado = new File(dir_invalidado);

				boolean sucesso = pendentes.renameTo(new File(invalidado, pendentes.getName()));
				if (sucesso) {
					System.out.println();
					System.out.println(pendentes.getName());
					System.out.println(
							"O arquivo é inválido e foi movido para a pasta:  '" + invalidado.getAbsolutePath() + "'");
					System.out.println();
				} else {
					System.out.println(pendentes.getName());
					System.out.println("Erro ao mover arquivo '" + pendentes.getAbsolutePath() + "' para '"
							+ invalidado.getAbsolutePath() + "'");
					System.out.println();
				}
				System.out.println("Verifique o arquivo adicionado e tente novamente ");
				System.out.println("-----------------------------------------------------");

			}
		}
		}
		finally {

			
			if (!pendentes.getName().contains("csv")) {
				pendentes.getName().substring(pendentes.getName().lastIndexOf(".") + 1);
				if(!pendentes.isDirectory());
			}else {
			
			
			if (lista.isEmpty()) {
				if (!pendentes.exists()) {

				} else {
					// *ADICIONE ABAIXO O CAMINHO DA PASTA INVALIDADO * //
					File invalidado = new File(dir_invalidado);

					boolean sucesso = pendentes.renameTo(new File(invalidado, pendentes.getName()));
					if (sucesso) {
						System.out.println();
						System.out.println(pendentes.getName());
						System.out.println("O arquivo está vazio portanto é inválido e foi movido para a pasta:  '"
								+ invalidado.getAbsolutePath() + "'");
						System.out.println();
					} else {
						System.out.println();
						System.out.println(pendentes.getName());
						System.out.println("Erro ao mover arquivo '" + pendentes.getAbsolutePath() + "' para '"
								+ invalidado.getAbsolutePath() + "'");
						System.out.println();
					}

					System.out.println("-----------------------------------------------------");
				}

			}
			
		}
		}
		
		if (!pendentes.getName().contains("csv")) {
			pendentes.getName().substring(pendentes.getName().lastIndexOf(".") + 1);
			if(!pendentes.isDirectory())
				System.out.println("O arquivo " + pendentes.getName() +  " não está no formato CSV. Tente outro arquivo!");
				System.out.println();
		}else {
		
		if (lista.containsAll(lista)) {

			if (!pendentes.exists()) {

			} else {
				// * ADICIONE ABAIXO O CAMINHO DA PASTA VALIDADO * //
				File Validado = new File(dir_validado);

				boolean sucesso = pendentes.renameTo(new File(Validado, pendentes.getName()));
				if (sucesso) {
					System.out.println();
					System.out.println(pendentes.getName());
					System.out.println("Arquivo aceito e movido para '" + Validado.getAbsolutePath() + "'");
					System.out.println();
				} else {
					System.out.println();
					System.out.println(pendentes.getName());
					System.out.println("Erro ao mover arquivo '" + pendentes.getAbsolutePath() + "' para '"
							+ Validado.getAbsolutePath() + "'");
					System.out.println();
				}

				System.out.println("-----------------------------------------------------");
			}
		}
		
		}
		sc.close();

	}

	public void processarDiretorio(String caminho_diretorio) throws ParseException, IOException {
		final File folder = new File(caminho_diretorio);
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {

			} else {
				salvarArquivo(fileEntry.getPath());
			}
		}

	}

}
